import React, { useEffect, useContext, useState } from 'react';
import config from 'visual-config-exposer';
import Database from 'database-api';

import { GameContext } from '../../context/gameContext';
import { ScoreContext } from '../../context/scoreContext';
import LeaderBoard from '../LeaderBoard/LeaderBoard';
import './postQuiz.css';

const PostQuiz = () => {
  const [showScores, setShowScores] = useState(false);

  const database = new Database();

  const scoreContext = useContext(ScoreContext);
  const gameContext = useContext(GameContext);

  const playAgain = () => {
    gameContext.setMainScreen();
    scoreContext.resetScore(0);
  };

  const submitHandler = () => {
    const username = document.getElementById('username').value;
    const score = scoreContext.score;
    const scoreData = {
      display_name: username,
      score: score,
    };
    database.postScoreData(scoreData).then(() => {
      setShowScores(true);
    });
  };

  const changeLeaderBoardHandler = () => {
    setShowScores(false);
  };

  return (
    <div>
      {!showScores ? (
        <article className="pre__card">
          <div>
            <h1
              className="post__title"
              style={{ color: config.postQuizScreen.scoreTextColor }}
            >
              {config.postQuizScreen.scoreText} : {scoreContext.score}
            </h1>
          </div>
          <div className="submit">
            <label htmlFor="username" className="submit_label" />
            <input
              type="text"
              placeholder="Username"
              className="submit_input"
              name="username"
              id="username"
            />
            <button className="cta-button" onClick={submitHandler}>
              Submit Score
            </button>
          </div>
          <div className="pre__btn-container">
            <button
              className="button"
              onClick={playAgain}
              style={{
                color: config.postQuizScreen.againButtonTextColor,
                backgroundColor: config.postQuizScreen.againButtonColor,
              }}
            >
              {config.postQuizScreen.againButtonText}
            </button>
          </div>
          <div className="cta">
            <h4 className="cta-text">{config.postQuizScreen.ctaText}</h4>
            <a
              href={config.postQuizScreen.ctaUrl}
              target="_blank"
              rel="noopener noreferer"
              className="cta-link"
            >
              <button className="cta-button">
                {config.postQuizScreen.ctaBtnText}
              </button>
            </a>
          </div>
        </article>
      ) : (
        <article className="pre__card">
          <LeaderBoard />
          <LeaderBoardButton
            className="button"
            onClick={changeLeaderBoardHandler}
          >
            back
          </LeaderBoardButton>
        </article>
      )}
    </div>
  );
};

export default PostQuiz;

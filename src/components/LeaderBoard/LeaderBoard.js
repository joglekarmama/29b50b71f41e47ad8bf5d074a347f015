import React, { useEffect, useState } from 'react';
import Database from 'database-api';

let database = new Database();

const LeaderBoard = () => {
  let Data;
  const [data, setData] = useState([]);

  useEffect(() => {
    getLeaderBoardHandler();
  }, []);

  const getLeaderBoardHandler = () => {
    try {
      database.getLeaderBoard().then((data) => {
        let sortedData = data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        setData(sortedData);
      });
    } catch (err) {
      throw new Error(err);
    }
  };

  if (data) {
    Data = data.map((element, index) => (
      <div key={index}>
        <li>{index + 1 + '. ' + element.display_name}</li>
        <li> {element.score}</li>
      </div>
    ));
  }

  return (
    <div>
      <h2>LeaderBoard</h2>
      <ul>{Data}</ul>
    </div>
  );
};

export default LeaderBoard;
